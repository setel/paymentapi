import bodyParser = require("body-parser");
import express = require("express");
import {Express, NextFunction, Request, Response} from "express";
import {PaymentController} from "./controller/payment.controller";
import {Database} from "./database";
import {authorization, process404} from "./service/controller.service";
const exp: Express = express();
const port = 4000;

new Database().init().then(() => {
    exp.use(bodyParser.json());
    exp.use(bodyParser.urlencoded({extended: false}));

    exp.use((request: Request, response: Response, next: NextFunction) => {
        response.header("Access-Control-Allow-Origin", "*");
        response.header("Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept");
        response.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        next();
    });

    exp.use(authorization);
    new PaymentController().init(exp);

    exp.all("*", process404);
    exp.listen( port, () => {
        console.log( `server started at http://localhost:${ port }` );
    });
});
