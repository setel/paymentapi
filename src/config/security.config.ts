export const permissions: Array<{url: string, method: string, role: string[]}> = [
    {
        method: "get",
        role: ["*"],
        url: ""
    }, {
        method: "post",
        role: ["admin", "customer"],
        url: "/payment"
    }
];
