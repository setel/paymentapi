import {Document, model, Model, Schema} from "mongoose";
import uniqueValidator = require("mongoose-unique-validator");
import {UserRole, UserStatus} from "../enum/user.enum";

export interface IUserModel extends Document {
    createdDate?: Date;
    email: string;
    role: UserRole;
    status: UserStatus;
}

const UserSchema = new Schema({
    createdDate: Date,
    email: {
        index: true,
        required: true,
        type: String,
        unique: true
    },
    role: {
        enum: Object.values(UserRole),
        required: true,
        type: String
    },
    status: {
        enum: Object.values(UserStatus),
        required: true,
        type: String
    }
});

UserSchema.plugin(uniqueValidator);
UserSchema.pre("save", (next) => {
    if (!this.date) {
        this.date = new Date();
    }
    next();
});

export const UserModel: Model<IUserModel> = model<IUserModel>("user", UserSchema);
