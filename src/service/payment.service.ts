import {IOrderModel, OrderModel, OrderStates} from "../database";

export const makePayment = async (orderId: string): Promise<boolean> => {
    const order: IOrderModel = await OrderModel.findById(orderId);
    if (order && order.states === OrderStates.CREATED) {
        // payment process
        const random = Math.floor(Math.random() * 2);

        if (random === 0) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
};
