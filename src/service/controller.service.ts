import {NextFunction, Request, Response} from "express";
import {permissions} from "../config/security.config";
import {IUserModel, UserModel} from "../database";
import {UserStatus} from "../database";
import {StatusCode} from "../enum/statusCode.enum";
import {IResponseError, IValidate} from "../typings/controller";

export const sendError = (response: Response, error: IResponseError): void => {
    response.status(error.code).format({
        "application/json": () => {
            response.send({
                error: {
                    code: error.code,
                    errors: error.errors,
                    message: error.message,
                    status: error.status
                }
            });
        }
    });
};

export const getUser = async (userId: string | undefined): Promise<IUserModel> => {
    if (userId) {
        try {
            return await UserModel.findById(userId);
        } catch (error) {
            console.error("User not found");
            return undefined;
        }
    } else {
        return undefined;
    }
};

const userAuth = async (user: IUserModel | undefined, response: Response, next: NextFunction, permission?: {
    url: string,
    method: string,
    role: string[]
}) => {
    if (user && user.status === UserStatus.ACTIVE && (permission === undefined ||
        (permission.role.some((role) => user.role && user.role.length >= 0 && user.role.indexOf(role) >= 0)))) {
        next();
    } else {
        sendError(response, {
            code: 403,
            errors: ["Unauthorized"],
            message: "Unauthorized",
            status: "FORBIDDEN"
        });
    }
};

const getPermission = (request: Request): {url: string, method: string, role: string[]} | undefined => {
    const urls: string[] = [];
    let url = request.url;

    if (url.endsWith("/")) {
        url = url.substring(0, url.length - 1);
    }

    if (url.lastIndexOf("?") >= 0) {
        url = url.substring(0, url.lastIndexOf("?"));
    }

    urls.push(url);
    const paths: string[] = url.substring(1).split("/");
    let path = "/";

    for (let i = 0; i < paths.length - 1; i ++) {
        path += paths[i] + "/";
        urls.push(path + "**");
    }

    return permissions.find((item) =>
        urls.indexOf(item.url) >= 0 && item.method === request.method.toLowerCase()
    );
};

export const authorization = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    if (request.method.toLowerCase() === "options") {
        next();
    } else {
        const permission = getPermission(request);
        if (permission && permission.role && permission.role.length === 1 && permission.role[0] === "*") {
            next();
        } else if (permission && permission.role && permission.role.length > 0 && permission.role[0] !== "*") {
            await userAuth(await getUser(request.headers.authorization), response, next, permission);
        } else {
            await userAuth(await getUser(request.headers.authorization), response, next);
        }
    }
};

export const controllerProcess = async (validate: IValidate | IValidate[],
                                        exec: () => void, response: Response): Promise<void> => {
    const valid = (validate instanceof Array) ? validate.find((v) => !v.valid) : validate;
    const validation = valid ? valid.valid : true;
    const validationResp = valid ? valid.resp : "";

    if (validation) {
        try {
            response.send(await exec());
        } catch (error) {
            console.error(error);

            if (error.message.indexOf("|") >= 0) {
                const msg: string[] = error.message.split("|");
                const code = Number(msg[0]);

                sendError(response, {
                    code,
                    errors: [msg[1]],
                    message: msg[1],
                    status: StatusCode[code]
                });
            } else {
                sendError(response, {
                    code: StatusCode.INTERNAL,
                    errors: [error.message],
                    message: error.message,
                    status: StatusCode[StatusCode.INTERNAL]
                });
            }
        }
    } else {
        if (typeof validationResp === "string") {
            sendError(response, {
                code: StatusCode.BAD_REQUEST,
                errors: [validationResp],
                message: validationResp,
                status: StatusCode[StatusCode.BAD_REQUEST]
            });
        } else {
            validationResp();
        }
    }
};

export const process404 = (request: Request, response: Response, next: NextFunction): void => {
    if (request.method.toLowerCase() === "options") {
        next();
    } else {
        const permission = getPermission(request);
        if (permission) {
            next();
        } else {
            sendError(response, {
                code: StatusCode.NOT_FOUND,
                errors: ["End point does not exist"],
                message: "End point does not exist",
                status: StatusCode[StatusCode.NOT_FOUND],
            });
        }
    }
};
