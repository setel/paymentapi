import {Express, Request, Response} from "express";
import {controllerProcess} from "../service/controller.service";
import {makePayment} from "../service/payment.service";
import {IValidate} from "../typings/controller";
import {IPaymentRequest} from "../typings/payment";

export class PaymentController {
    public init(express: Express) {
        express.get("/", async (request: Request, response: Response) => {
            const validate: IValidate = {
                resp: "",
                valid: true
            };

            const exec = async () => "PaymentAPI is up";
            await controllerProcess(validate, exec, response);
        });

        express.post("/payment", async (request: Request, response: Response) => {
            const paymentRequest = request.body as IPaymentRequest;
            const validate: IValidate = {
                resp: "order id is required",
                valid: paymentRequest.orderId
            };

            const exec = async () => makePayment(paymentRequest.orderId);
            await controllerProcess(validate, exec, response);
        });
    }
}
