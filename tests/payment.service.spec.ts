import {expect} from "chai";
import {Database, IOrderModel, OrderModel, OrderStates} from "../src/database";
import {makePayment} from "../src/service/payment.service";

const clearData = async () => {
    // clean database with productId === "productId"
    await OrderModel.deleteMany({productId: "productId"});
};

describe("Test payment service", () => {
    it("should be failed to make payment", async () => {
        await new Database().init();
        await clearData();
        let success = false;

        try {
            await makePayment("withoutThisId");
            success = true;
        } catch (error) {
            success = false;
        }

        expect(success).to.equal(false);
    });

    it("should be able to make payment", async () => {
        const productId = "productId";
        const productName = "productName";
        const price = 500;
        const userId = "5c8a0a6955c24f5d2cb8d7db";
        const orderStates = OrderStates.CREATED;

        const order: IOrderModel = new OrderModel({
            productId: productId,
            productName: productName,
            price: price,
            states: orderStates,
            userId: userId
        });

        await order.save();

        expect(order.productId).to.equal(productId);
        expect(order.productName).to.equal(productName);
        expect(order.states).to.equal(orderStates);
        expect(order.price).to.equal(price);

        let success = false;

        try {
            await makePayment(order.id);
            success = true;
        } catch (error) {
            success = false;
        }

        expect(success).to.equal(true);
        await clearData();
    });
});
