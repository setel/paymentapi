# paymentapi

## Project setup
```
npm install
```

### Run the project
```
npm run start
```

### Compiles for production
```
npm run build
```

### Run your tests
```
npm run test
```

